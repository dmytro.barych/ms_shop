<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    public function definition(): array
    {
        return [
            'info' => $this->faker->text,
            'user_id' => 2,
            'sum' => $this->faker->numberBetween(),
        ];
    }
}
