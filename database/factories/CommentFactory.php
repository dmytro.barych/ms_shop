<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class CommentFactory extends Factory
{
    public function definition(): array
    {
        return [
            'text' => $this->faker->text,
            'user_id' => 2,
        ];
    }
}
