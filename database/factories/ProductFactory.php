<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class ProductFactory extends Factory
{
    public function definition(): array
    {
        return [
            'title' => $this->faker->title,
            'currency' => Order::DEFAULT_CURRENCY,
            'description' => $this->faker->text,
            'price' => $this->faker->randomDigitNotZero(),
            'archive' => false,
            'status' => $this->faker->randomElement(Product::STATUSES),
            'type' => $this->faker->randomElement(Product::TYPES)
        ];
    }
}
