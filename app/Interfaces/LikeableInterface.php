<?php

declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface LikeableInterface
{
    public function likes(): MorphMany;
}
