<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    public function toArray($request): array
    {
        $orderable = $this->orderable instanceof Product
            ? new ProductResource($this->orderable)
            : new VoucherResource($this->orderable);
        return [
            'id' => $this->id,
            'count' => $this->count,
            'ordarable' => $orderable
        ];
    }
}
