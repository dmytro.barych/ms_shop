<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Complaint;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ComplainCollection extends ResourceCollection
{
    public $collection = Complaint::class;
}
