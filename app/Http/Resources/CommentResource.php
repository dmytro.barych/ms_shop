<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'product_id' => $this->product_id,
            'text' => $this->text,
            'likes_count' => $this->likes()->count(),
            'childes' => new CommentCollection($this->childes)
        ];
    }
}
