<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\ToggleLikeAction;
use App\Actions\Comments\CommentDeleteAction;
use App\Actions\Comments\CommentStoreAction;
use App\Actions\Comments\CommentUpdateAction;
use App\Http\Requests\IndexCommentRequest;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    public function index(IndexCommentRequest $request): CommentCollection
    {
        return new CommentCollection(
            Comment::orderBy('created_at', 'desc')
                ->whereNull('parent_id')
                ->whereIn('product_id', $request->product_ids)
                ->withChildes(3)
                ->paginate($request->per_page ?? 3)
        );
    }

    public function store(StoreCommentRequest $request, CommentStoreAction $action): CommentResource
    {
        return new CommentResource($action->handle($request->validated()));
    }

    public function show(Comment $comment): CommentResource
    {
        return new CommentResource(
            $comment->load([
                'childes' => function ($q) {
                    $q->orderBy('created_at')->limit(10);
                }
            ])
        );
    }

    public function showChildes(Comment $comment): CommentCollection
    {
        return new CommentCollection(Comment::where('parent_id', $comment->id)->orderBy('created_at')->paginate(10));
    }

    public function update(UpdateCommentRequest $request, Comment $comment, CommentUpdateAction $action):
    CommentResource {
        return new CommentResource($action->handle($comment, $request->validated()));
    }

    public function destroy(Comment $comment, CommentDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($comment), Response::HTTP_OK]);
    }

    public function toggleLike(Comment $comment, int $userId, ToggleLikeAction $action): CommentResource
    {
        return new CommentResource($action->handle($comment, $userId));
    }
}
