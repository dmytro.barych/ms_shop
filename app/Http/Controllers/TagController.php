<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\IndexTagRequest;
use App\Http\Resources\TagCollection;
use App\Models\Tag;

class TagController extends Controller
{
    public function index(IndexTagRequest $request): TagCollection
    {
        $tagQuery = $request->search ? Tag::search($request->search ?? null) : Tag::query();
        return new TagCollection($tagQuery->paginate(10));
    }
}
