<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Product\ProductDeleteAction;
use App\Actions\Product\ProductStoreAction;
use App\Actions\Product\ProductUpdateAction;
use App\Http\Requests\IndexProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    public function index(IndexProductRequest $request): ProductCollection
    {
        $productQuery = $request->search ? Product::search($request->search ?? null) : Product::query();
        return new ProductCollection($productQuery->paginate());
    }

    public function store(StoreProductRequest $request, ProductStoreAction $action): ProductResource
    {
        return new ProductResource($action->handle($request->validated()));
    }

    public function show(Product $product): ProductResource
    {
        return new ProductResource($product);
    }

    public function update(UpdateProductRequest $request, Product $product, ProductUpdateAction $action):
    ProductResource {
        return new ProductResource($action->handle($product, $request->validated()));
    }

    public function destroy(Product $product, ProductDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($product)]);
    }

    public function getProductsByTag(Tag $tag): ProductCollection
    {
        return new ProductCollection($tag->products()->paginate());
    }

    public function getProductsByCategory(Category $category): ProductCollection
    {
        return new ProductCollection($category->products()->paginate());
    }
}
