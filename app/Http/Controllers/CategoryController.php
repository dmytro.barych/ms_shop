<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Category\CategoryDeleteAction;
use App\Actions\Category\CategoryStoreAction;
use App\Actions\Category\CategoryUpdateAction;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    public function index(): CategoryCollection
    {
        $categories = Category::whereIsRoot()->with('children')->paginate(10);
        $categories->getCollection()->transform(function ($category) {
            $category->children = Category::descendantsOf($category->id)->toTree();
            return $category;
        });
        return new CategoryCollection($categories);
    }

    public function store(StoreCategoryRequest $request, CategoryStoreAction $action): CategoryResource
    {
        return new CategoryResource($action->handle($request->validated()));
    }

    public function show(Category $category): CategoryResource
    {
        return new CategoryResource(Category::descendantsAndSelf($category->id)->toTree()->first());
    }

    public function update(UpdateCategoryRequest $request, Category $category, CategoryUpdateAction $action):
    CategoryResource {
        return new CategoryResource($action->handle($category, $request->validated()));
    }

    public function destroy(Category $category, CategoryDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($category)]);
    }
}
