<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\ToggleLikeAction;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductLikeController extends Controller
{
    public function toggleLike(Product $product, ToggleLikeAction $action, int $userId): ProductResource
    {
        return new ProductResource($action->handle($product, $userId));
    }

    public function wishList(int $userId): ProductCollection
    {
        return new ProductCollection(
            Product::whereHas('likes', function ($q) use ($userId) {
                $q->where('user_id', $userId);
            })->paginate()
        );
    }
}
