<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Voucher\VoucherDeleteAction;
use App\Actions\Voucher\VoucherStoreAction;
use App\Actions\Voucher\VoucherUpdateAction;
use App\Http\Requests\StoreVoucherRequest;
use App\Http\Requests\UpdateVoucherRequest;
use App\Http\Resources\VoucherCollection;
use App\Http\Resources\VoucherResource;
use App\Models\Voucher;
use Illuminate\Http\JsonResponse;

class VoucherController extends Controller
{
    public function index(): VoucherCollection
    {
        return new VoucherCollection(Voucher::query()->paginate());
    }

    public function store(StoreVoucherRequest $request, VoucherStoreAction $action): VoucherResource
    {
        return new VoucherResource($action->handle($request->validated()));
    }

    public function show(Voucher $voucher): VoucherResource
    {
        return new VoucherResource($voucher);
    }

    public function update(UpdateVoucherRequest $request, Voucher $voucher, VoucherUpdateAction $action):
    VoucherResource {
        return new VoucherResource($action->handle($voucher, $request->validated()));
    }

    public function destroy(Voucher $voucher, VoucherDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($voucher)]);
    }
}
