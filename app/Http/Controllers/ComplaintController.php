<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Complaint\ComplaintDeleteAction;
use App\Actions\Complaint\ComplaintStoreAction;
use App\Actions\Complaint\ComplaintToggleChecked;
use App\Http\Requests\StoreComplaintRequest;
use App\Http\Resources\ComplainCollection;
use App\Http\Resources\ComplainResource;
use App\Models\Complaint;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ComplaintController extends Controller
{
    public function index(): ComplainCollection
    {
        return new ComplainCollection(Complaint::orderBy('created_at', 'desc')->paginate());
    }

    public function store(StoreComplaintRequest $request, ComplaintStoreAction $action): ComplainResource
    {
        return new ComplainResource($action->handle($request->validated()));
    }

    public function show(Complaint $complaint): ComplainResource
    {
        return new ComplainResource($complaint);
    }

    public function toggleChecked(Complaint $complaint, ComplaintToggleChecked $action): ComplainResource
    {
        return new ComplainResource($action->handle($complaint));
    }

    public function destroy(Complaint $complaint, ComplaintDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($complaint), Response::HTTP_OK]);
    }
}
