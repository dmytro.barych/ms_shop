<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\MakePaymentRequest;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;

class PaymentsController extends Controller
{
    public function make(Order $order, MakePaymentRequest $request, OrderService $orderService): JsonResponse
    {
        return response()->json(['success' => $orderService->makePayment($order, $request->validated())]);
    }
}
