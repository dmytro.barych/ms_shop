<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Order\OrderDeleteAction;
use App\Actions\Order\OrderStoreAction;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    private OrderService $orderService;

    public function __construct(OrderService $service)
    {
        $this->orderService = $service;
    }

    public function index(): OrderCollection
    {
        return new OrderCollection(Order::with('items')->paginate());
    }

    public function store(StoreOrderRequest $request, OrderStoreAction $action): OrderResource
    {
        $data = $request->validated();
        $data['sum'] = $this->orderService->calculate($data);
        return new OrderResource($action->handle($data));
    }

    public function show(Order $order): OrderResource
    {
        return new OrderResource($order->load('items'));
    }

    public function destroy(Order $order, OrderDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($order)]);
    }
}
