<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCommentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => [
                'required',
                'integer',
                'exists:products,id'
            ],
            'user_id' => [
                'required',
                'integer'
            ],
            'text' => [
                'sometimes',
                'min:3'
            ],
            'rate' => [
                'sometimes',
                'integer',
                'digits_between:1,5'
            ],
            'parent_id' => [
                'sometimes',
                Rule::exists('comments', 'id')
                    ->where('product_id', $this->product_id)
                    ->whereNull('parent_id')
            ]
        ];
    }
}
