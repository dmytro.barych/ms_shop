<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreComplaintRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
                'integer'
            ],
            'title' => [
                'required',
                'min:3'
            ],
            'text' => [
                'required',
                'min:3'
            ]
        ];
    }
}
