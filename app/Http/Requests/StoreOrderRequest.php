<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'info' => [
                'sometimes',
            ],
            'user_id' => [
                'required',
                'integer'
            ],
            'voucher_codes' => [
                'sometimes',
                'array',
            ],
            'voucher_codes.*' => [
                'sometimes',
                'exists:vouchers,code',
            ],
            'product_ids' => [
                'required',
                'array'
            ],
            'product_ids.*' => [
                'required',
                'exists:products,id',
            ],
        ];
    }
}
