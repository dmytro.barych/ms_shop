<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Models\Voucher;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreVoucherRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in(Voucher::TYPES)
            ],
            'discount' => [
                'required',
                'integer',
            ],
            'sign' => [
                'required',
                Rule::in(Voucher::SIGNS)
            ],
            'product' => [
                'required',
            ]
        ];
    }
}
