<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexCommentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product_ids' => [
                'required',
                'array'
            ],
            'product_ids.*' => [
                'required',
                'exists:products,id',
            ],
            'per_page' => [
                'sometimes',
                'integer'
            ]
        ];
    }
}
