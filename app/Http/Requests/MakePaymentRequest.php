<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MakePaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'payload' => [
                'array',
                'required'
            ],
            'payload.nonce' => [
                'required',
            ]
        ];
    }
}
