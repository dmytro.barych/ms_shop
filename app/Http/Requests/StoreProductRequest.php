<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'status' => [
                'sometimes',
                Rule::in(Product::STATUSES)
            ],
            'title' => [
                'required',
                'min:3',
                'unique_translation:products'
            ],
            'description' => [
                'sometimes',
                'unique_translation:products'
            ],
            'category_ids' => [
                'sometimes',
                'array'
            ],
            'category_ids.*' => [
                'sometimes',
                'integer',
                'exists:categories,id'
            ],
            'tags' => [
                'sometimes',
                'array',
            ],
            'tags.*' => [
                'sometimes',
                'min:3',
                'alpha_dash'
            ],
            'price' => [
                'required',
                'regex:/^[0-9]+(\.[0-9][0-9]?)?$/'
            ],
            'currency' => [
                'required',
            ]
        ];
    }
}
