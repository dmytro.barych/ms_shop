<?php

declare(strict_types=1);

namespace App\Actions\Voucher;

use App\Models\Voucher;

class VoucherUpdateAction
{
    public function handle(Voucher $voucher, array $data): Voucher
    {
        $voucher->fill($data);
        $voucher->save();

        return $voucher;
    }
}
