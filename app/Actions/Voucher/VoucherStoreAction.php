<?php

declare(strict_types=1);

namespace App\Actions\Voucher;

use App\Models\Voucher;

class VoucherStoreAction
{
    public function handle(array $data): Voucher
    {
        $voucher = new Voucher($data);
        $voucher->save();

        return $voucher;
    }
}
