<?php

declare(strict_types=1);

namespace App\Actions\Voucher;

use App\Models\Voucher;

class VoucherDeleteAction
{
    public function handle(Voucher $voucher): bool
    {
        return $voucher->delete();
    }
}
