<?php

declare(strict_types=1);

namespace App\Actions\Comments;

use App\Models\Comment;

class CommentUpdateAction
{
    public function handle(Comment $comment, array $data): Comment
    {
        $comment->fill($data);
        $comment->save();

        return $comment;
    }
}
