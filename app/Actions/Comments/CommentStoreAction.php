<?php

declare(strict_types=1);

namespace App\Actions\Comments;

use App\Models\Comment;

class CommentStoreAction
{
    public function handle(array $data): Comment
    {
        $comment = new Comment($data);
        $comment->product()->associate($data['product_id']);
        $comment->parent()->associate($data['parent_id'] ?? null);
        $comment->save();

        return $comment;
    }
}
