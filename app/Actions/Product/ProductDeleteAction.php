<?php

declare(strict_types=1);

namespace App\Actions\Product;

use App\Models\Product;

class ProductDeleteAction
{
    public function handle(Product $product): bool
    {
        return $product->delete();
    }
}
