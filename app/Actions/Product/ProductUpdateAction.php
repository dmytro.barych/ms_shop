<?php

declare(strict_types=1);

namespace App\Actions\Product;

use App\Actions\Tag\TagsStoreAction;
use App\Exceptions\BasicException;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductUpdateAction
{
    public function handle(Product $product, array $data): ?Product
    {
        try {
            DB::beginTransaction();

            $product->fill($data);
            $product->save();

            $product->categories()->sync($data['category_ids'] ?? null);
            $action = app()->make(TagsStoreAction::class);
            $tags = $action->handle($data['tags'] ?? []);
            $product->tags()->sync($tags->pluck('id')->toArray());

            DB::commit();

            return $product;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
