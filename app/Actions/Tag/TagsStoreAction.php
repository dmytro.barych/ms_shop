<?php

declare(strict_types=1);

namespace App\Actions\Tag;

use App\Exceptions\BasicException;
use App\Models\Tag;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TagsStoreAction
{
    public function handle(array $tags): Collection
    {
        try {
            DB::beginTransaction();

            $result = [];
            foreach ($tags as $item) {
                $result[] = Tag::firstOrCreate(['name' => $item]);
            }

            DB::commit();

            return collect($result);
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
