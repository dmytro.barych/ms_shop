<?php

declare(strict_types=1);

namespace App\Actions\Category;

use App\Models\Category;

class CategoryUpdateAction
{
    public function handle(Category $category, array $data): Category
    {
        $category->fill($data);
        $category->save();

        return $category;
    }
}
