<?php

declare(strict_types=1);

namespace App\Actions\Category;

use App\Models\Category;

class CategoryStoreAction
{
    public function handle(array $data): Category
    {
        $category = new Category($data);
        $category->parent()->associate($data['parent_id'] ?? null);
        $category->save();

        return $category;
    }
}
