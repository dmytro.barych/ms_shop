<?php

declare(strict_types=1);

namespace App\Actions\Category;

use App\Models\Category;

class CategoryDeleteAction
{
    public function handle(Category $category): bool
    {
        return $category->delete();
    }
}
