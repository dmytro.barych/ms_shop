<?php

declare(strict_types=1);

namespace App\Actions\Order;

use App\Exceptions\BasicException;
use App\Models\Order;
use App\Models\Voucher;
use Illuminate\Support\Facades\DB;

class OrderStoreAction
{
    public function handle(array $data): ?Order
    {
        try {
            DB::beginTransaction();

            $order = new Order($data);
            $order->save();
            $itemsArray = $this->generateArrayForItems($data);
            $order->items()->createMany($itemsArray);

            DB::commit();

            return $order;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }

    private function generateArrayForItems(array $data): array
    {
        $result = [];
        $productsCounts = array_count_values($data['product_ids']);
        foreach ($productsCounts as $id => $count) {
            $result[] = ['orderable_id' => $id, 'orderable_type' => 'product', 'count' => $count];
        }
        $vouchers = Voucher::whereIn('code', $data['voucher_codes'])->orderBy('type')->get()->unique('type');
        foreach ($vouchers as $voucher) {
            $result[] = [
                'orderable_id' => $voucher->id,
                'orderable_type' => 'voucher',
                'count' => 1
            ];
        }
        return $result;
    }
}
