<?php

declare(strict_types=1);

namespace App\Actions\Order;

use App\Models\Order;

class OrderDeleteAction
{
    public function handle(Order $order): bool
    {
        return $order->delete();
    }
}
