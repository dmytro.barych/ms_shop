<?php

declare(strict_types=1);

namespace App\Actions;

use App\Interfaces\LikeableInterface;

class ToggleLikeAction
{
    public function handle(LikeableInterface $likeable, int $userId): LikeableInterface
    {
            $like = $likeable->likes()->where('user_id', $userId)->first();
            if ($like) {
                $like->delete();
            } else {
                $likeable->likes()->create(['user_id' => $userId]);
            }

            return $likeable;
    }
}
