<?php

declare(strict_types=1);

namespace App\Actions\Complaint;

use App\Models\Complaint;

class ComplaintDeleteAction
{
    public function handle(Complaint $complaint): bool
    {
        return $complaint->delete();
    }
}
