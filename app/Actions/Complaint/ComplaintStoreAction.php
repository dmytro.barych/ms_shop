<?php

declare(strict_types=1);

namespace App\Actions\Complaint;

use App\Models\Complaint;

class ComplaintStoreAction
{
    public function handle(array $data): Complaint
    {
        $complaint = new Complaint($data);
        $complaint->save();
        return $complaint;
    }
}
