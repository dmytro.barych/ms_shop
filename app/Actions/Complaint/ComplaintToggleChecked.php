<?php

declare(strict_types=1);

namespace App\Actions\Complaint;

use App\Models\Complaint;

class ComplaintToggleChecked
{
    public function handle(Complaint $complaint): Complaint
    {
        $complaint->checked = !$complaint->checked;
        $complaint->save();

        return $complaint;
    }
}
