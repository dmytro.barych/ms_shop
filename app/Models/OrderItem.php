<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\OrderItemRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;
    use OrderItemRelationsTrait;

    protected $fillable = ['count', 'orderable_type', 'orderable_id'];
}
