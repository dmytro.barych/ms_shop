<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait OrderRelationsTrait
{
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
