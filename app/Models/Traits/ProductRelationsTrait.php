<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\Category;
use App\Models\Comment;
use App\Models\OrderItem;
use App\Models\Like;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait ProductRelationsTrait
{
    public function likes(): MorphMany
    {
        return $this->morphMany(Like::class,'likeable');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'product_tags');
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function orderItems(): MorphMany
    {
        return $this->morphMany(OrderItem::class,'orderable');
    }
}
