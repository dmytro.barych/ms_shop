<?php

declare(strict_types=1);

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Relations\MorphTo;

trait ProductLikeRelationsTrait
{
    public function likeable(): MorphTo
    {
        return $this->morphTo();
    }
}
