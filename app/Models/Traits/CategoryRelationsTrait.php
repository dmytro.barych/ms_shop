<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait CategoryRelationsTrait
{
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_categories');
    }
}
