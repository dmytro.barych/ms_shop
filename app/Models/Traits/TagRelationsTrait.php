<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait TagRelationsTrait
{
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_tags');
    }
}
