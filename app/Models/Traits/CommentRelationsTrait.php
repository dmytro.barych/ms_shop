<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\Comment;
use App\Models\Product;
use App\Models\Like;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait CommentRelationsTrait
{
    public function likes(): MorphMany
    {
        return $this->morphMany(Like::class,'likeable');
    }

    public function childes(): HasMany
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    public function parent(): ?BelongsTo
    {
        return $this->belongsTo(Comment::class, 'parent_id', 'id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
