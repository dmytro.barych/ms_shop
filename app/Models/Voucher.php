<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'type', 'discount', 'sign', 'product'];

    public const TYPES = [self::TYPE_R, self::TYPE_S, self::TYPE_V];
    public const TYPE_V = 'v';
    public const TYPE_R = 'r';
    public const TYPE_S = 's';

    public const SIGNS = [self::PERCENT, self::DOLLAR];
    public const PERCENT = '%';
    public const DOLLAR = 'r';
}
