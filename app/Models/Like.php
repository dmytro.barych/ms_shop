<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\ProductLikeRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use HasFactory;
    use ProductLikeRelationsTrait;

    protected $fillable = ['user_id'];
}
