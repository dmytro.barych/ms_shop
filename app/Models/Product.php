<?php

declare(strict_types=1);

namespace App\Models;

use App\Interfaces\LikeableInterface;
use App\Models\Traits\ProductRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Spatie\Translatable\HasTranslations;

class Product extends Model implements LikeableInterface
{
    use HasFactory;
    use Searchable;
    use HasTranslations;
    use ProductRelationsTrait;

    protected $fillable = ['title', 'description', 'price', 'archive', 'status', 'currency', 'type'];

    public array $translatable = ['title', 'description'];

    protected $appends = ['rate'];

    const STATUSES = [self::AVAILABLE_STATUS, self::END_SOON_STATUS, self::END_STATUS];
    const AVAILABLE_STATUS = 'Available';
    const END_SOON_STATUS = 'End soon';
    const END_STATUS = 'End';

    public const TYPES = [self::TYPE_A, self::TYPE_B, self::TYPE_C];
    public const TYPE_A = 'a';
    public const TYPE_B = 'b';
    public const TYPE_C = 'c';

    public function getRateAttribute(): float
    {
        $rateAr = $this->comments()->whereNotNull('rate')->pluck('rate')->toArray();
        return !empty($rateAr) ? round(array_sum($rateAr) / count($rateAr), 2) : 0;
    }
}
