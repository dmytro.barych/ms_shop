<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\CategoryRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory;
    use HasTranslations;
    use NodeTrait;
    use CategoryRelationsTrait;

    protected $fillable = ['slug', 'name'];

    public array $translatable = ['name'];
}
