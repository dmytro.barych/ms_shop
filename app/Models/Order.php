<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\OrderRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use OrderRelationsTrait;
    use SoftDeletes;

    public const DEFAULT_CURRENCY = 'USD';

    public const STATUSES = [self::STATUS_PENDING, self::STATUS_PAYED, self::STATUS_FAILED];
    public const STATUS_PENDING = 'pending';
    public const STATUS_PAYED = 'payed';
    public const STATUS_FAILED = 'failed';

    protected $fillable = ['info', 'sum', 'user_id'];
}
