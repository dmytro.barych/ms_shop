<?php

declare(strict_types=1);

namespace App\Models;

use App\Interfaces\LikeableInterface;
use App\Models\Traits\CommentRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model implements LikeableInterface
{
    use HasFactory;
    use CommentRelationsTrait;

    protected $fillable = ['text', 'user_id', 'rate'];

    public function scopeWithChildes($query, $perPage = 3)
    {
        return $query->with('childes', function ($q) use ($perPage) {
            $q->orderBy('created_at')->limit($perPage);
        });
    }
}
