<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Category;
use App\Models\Voucher;
use Illuminate\Support\Str;

class VoucherObserver
{
    public function creating(Voucher $voucher): void
    {
        $voucher->code = Str::random(8);
    }
}
