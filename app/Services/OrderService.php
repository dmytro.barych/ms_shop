<?php

declare(strict_types=1);

namespace App\Services;

use AmrShawky\Currency;
use App\Models\Order;
use App\Models\Product;
use App\Models\Voucher;
use Braintree\Transaction;

class OrderService
{
    public function calculate(array $data): float
    {
        $vouchers = Voucher::whereIn('code', $data['voucher_codes'])->orderBy('type')->get()->unique('type');
        $productsCounts = array_count_values($data['product_ids']);
        $productList = Product::whereIn('id', $data['product_ids'])
            ->get();
        $productsGroupedByType = $productList->groupBy('type');
        $productSum = $productList->map(function ($item) use ($productsCounts) {
            return $this->convertedPrice($item->price, $item->currency) * $productsCounts[$item->id];
        })->sum();

        foreach ($vouchers as $voucher) {
            switch ($voucher->type) {
                case Voucher::TYPE_V:
                    $products = $productsGroupedByType[$voucher->product] ?? null;
                    foreach ($products as $product) {
                        $usedForCount = intdiv($productsCounts[$product->id], 2);
                        if ($usedForCount) {
                            $productSum -= $usedForCount * $this->calculateDiscount(
                                    $this->convertedPrice($product->price, $product->currency),
                                    $voucher
                                );
                        }
                    }
                    break;
                case Voucher::TYPE_R:
                    $products = $productsGroupedByType[$voucher->product] ?? null;
                    foreach ($products as $product) {
                        $productSum -= $this->calculateDiscount(
                            $this->convertedPrice($product->price, $product->currency),
                            $voucher
                        );
                    }
                    break;
                case Voucher::TYPE_S:
                    if ($voucher->product < $productSum) {
                        $productSum -= $this->calculateDiscount($productSum, $voucher);
                    }
            }
        }
        return max($productSum, 0);
    }

    private function calculateDiscount($price, $voucher)
    {
        if ($voucher->sign === '%') {
            $result = (($price / 100) * $voucher->discount);
        } else {
            $result = $voucher->discount;
        }
        return $result;
    }

    public function convertedPrice($price, $currency)
    {
        if ($currency !== Order::DEFAULT_CURRENCY) {
            $price = Currency::convert()
                ->from($currency)
                ->to(Order::DEFAULT_CURRENCY)
                ->amount($price)
                ->round(2)
                ->get();
        }
        return $price;
    }

    public function makePayment(Order $order, array $data): bool
    {
        $result = Transaction::sale([
            'amount' => $order->sum,
            'paymentMethodNonce' => $data['payload']['nonce'],
            'options' => [
                'submitForSettlement' => true
            ]
        ]);
        if ($status = $result->success) {
            $order->status = Order::STATUS_PAYED;
        } else {
            $order->status = Order::STATUS_FAILED;
        }
        $order->save();
        return $status;
    }
}
