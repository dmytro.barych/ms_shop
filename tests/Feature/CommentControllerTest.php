<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeCommentTest()
    {
        $product = Product::factory()->create();
        $commentArray = [
            'user_id' => 1,
            'product_id' => $product->id,
            'text' => $this->faker()->text,
        ];
        $response = $this->post(route('comments.store', $commentArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $comment = $response->getOriginalContent();
        $this->assertDatabaseHas(Comment::class, [
            'id' => $comment->id,
            'text' => $commentArray['text'],
            'product_id' => $product->id
        ]);

        $commentArray = [
            'user_id' => 1,
            'product_id' => $product->id,
            'text' => $this->faker()->text,
            'parent_id' => $comment->id,
        ];
        $response = $this->post(route('comments.store', $commentArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $comment = $response->getOriginalContent();
        $this->assertDatabaseHas(Comment::class, [
            'id' => $comment->id,
            'text' => $commentArray['text'],
            'product_id' => $commentArray['product_id']
        ]);
    }

    /**
     * @test
     */
    public function updateCommentTest()
    {
        $commentOld = Comment::factory()->for(Product::factory()->create())->create();
        $commentArray = [$commentOld->id, 'text' => $this->faker()->text];
        $response = $this->put(route('comments.update', $commentArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Comment::class, [
            'id' => $commentOld->id,
            'text' => $commentArray['text'],
            'user_id' => $commentOld->user_id,
            'product_id' => $commentOld->product_id,
        ]);
    }

    /**
     * @test
     */
    public function indexCommentTest()
    {
        $product = Product::factory()->create();
        Comment::factory()->for(Comment::factory()->for($product)->create(), 'parent')
            ->for($product)
            ->create();
        $response = $this->get(route('comments.index', ['product_ids' => [$product->id]]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    [
                        'id',
                        'user_id',
                        'product_id',
                        'text',
                        'likes_count',
                        'childes' => [
                            [
                                'id',
                                'user_id',
                                'product_id',
                                'text',
                                'likes_count',
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function showCommentTest()
    {
        $product = Product::factory()->create();
        $comment = Comment::factory()->for($product)->create();
        Comment::factory()->for($comment, 'parent')->for($product)->create();
        $response = $this->get(route('comments.show', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'user_id',
                    'product_id',
                    'text',
                    'likes_count',
                    'childes' => [
                        [
                            'id',
                            'user_id',
                            'product_id',
                            'text',
                            'likes_count',
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function showCommentChildesTest()
    {
        $product = Product::factory()->create();
        $comment = Comment::factory()->for($product)->create();
        Comment::factory()->for($comment, 'parent')->for($product)->create();
        $response = $this->get(route('comments.childes', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    [
                        'id',
                        'user_id',
                        'product_id',
                        'text',
                        'likes_count'
                    ]
                ]
            ]
        );
        $this->assertEquals($response->getOriginalContent()->first()->parent_id, $comment->id);
    }

    /**
     * @test
     */
    public function destroyCommentTest()
    {
        $product = Product::factory()->create();
        $comment = Comment::factory()->for($product)->create();
        $commentChild = Comment::factory()->for($comment, 'parent')->for($product)->create();
        $response = $this->delete(route('comments.destroy', [$comment->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
        $this->assertEquals($comment->id, $commentChild->parent_id);
        $commentChild->refresh();
        $this->assertNotEquals($comment->id, $commentChild->parent_id);
    }

    /**
     * @test
     */
    public function toggleCommentLikeTest()
    {
        $product = Product::factory()->create();
        $comment = Comment::factory()->for($product)->create();

        //Like
        $response = $this->post(route('comments.toggle-like', [$comment->id, $comment->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('likes', ['likeable_id' => $comment->id, 'user_id' => $comment->user_id]);

        //Remove like
        $response = $this->post(route('comments.toggle-like', [$comment->id, $comment->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('likes', ['likeable_id' => $comment->id, 'user_id' => $comment->user_id]);
    }
}
