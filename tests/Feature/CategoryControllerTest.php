<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeCategoryTest()
    {
        $categoryArray = [
            'name' => $this->faker()->name,
        ];
        $response = $this->post(route('categories.store', $categoryArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $category = $response->getOriginalContent();
        $this->assertDatabaseHas(Category::class, [
            'id' => $category->id,
        ]);
    }

    /**
     * @test
     */
    public function updateCategoryTest()
    {
        $categoryOld = Category::factory()->create();
        $categoryArray = [
            'name' => $this->faker()->name,
        ];;
        $response = $this->put(route('categories.update', [$categoryOld->id] + $categoryArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Category::class, [
            'id' => $categoryOld->id,
            'slug' => Str::slug($categoryArray['name'])
        ]);
    }

    /**
     * @test
     */
    public function indexCategoryTest()
    {
        Category::factory()->count(5)->create();
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'slug',
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function showCategoryTest()
    {
        $category = Category::factory()->create();
        $response = $this->get(route('categories.show', [$category->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'slug',
            ]
        ]);
    }

    /**
     * @test
     */
    public function deleteCategoryTest()
    {
        $categoryOld = Category::factory()->create();
        $response = $this->delete(route('categories.destroy', [$categoryOld->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(Category::class, [
            'id' => $categoryOld->id
        ]);
    }
}
