<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductLikeControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function toggleLikeProductTest()
    {
        $product = Product::factory()->create();

        //Like
        $response = $this->post(route('products.toggle-like', [$product->id, 2]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('likes', ['likeable_id' => $product->id, 'user_id' => 2]);

        //Remove like
        $response = $this->post(route('products.toggle-like', [$product->id, 2]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('likes', ['likeable_id' => $product->id, 'user_id' => $product->user_id]);
    }

    /**
     * @test
     * @depends toggleLikeProductTest
     */
    public function wishListProductTest()
    {
        $products = Product::factory()->count(5)->create();

        foreach ($products as $product) {
            $response = $this->post(route('products.toggle-like', [$product->id, 2]));
            $response->assertStatus(Response::HTTP_OK);
        }

        $response = $this->post(route('products.wish-list', [2]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'description',
                    'price',
                    'currency',
                    'rate',
                    'created_at'
                ]
            ]
        ]);
    }
}
