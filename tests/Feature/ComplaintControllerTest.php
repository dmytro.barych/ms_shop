<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Complaint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ComplaintControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeComplaintTest()
    {
        $complaintArray = [
            'title' => $this->faker()->jobTitle,
            'text' => $this->faker()->text,
            'user_id' => 2,
        ];
        $response = $this->post(route('complaints.store', $complaintArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $complaint = $response->getOriginalContent();
        $this->assertDatabaseHas(Complaint::class, [
            'id' => $complaint->id,
        ]);
    }

    /**
     * @test
     */
    public function toggleCheckComplaintTest()
    {
        $complaintOld = Complaint::factory()->create();
        $response = $this->post(route('complaints.toggle-check', [$complaintOld->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Complaint::class, [
            'id' => $complaintOld->id,
            'checked' => true
        ]);
    }

    /**
     * @test
     */
    public function indexCategoryTest()
    {
        Complaint::factory()->count(5)->create();
        $response = $this->get(route('complaints.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'user_id',
                    'title',
                    'text',
                    'created_at'
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function showCategoryTest()
    {
        $complaint = Complaint::factory()->create();
        $response = $this->get(route('complaints.show', [$complaint->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'user_id',
                'title',
                'text',
                'created_at'
            ]
        ]);
    }

    /**
     * @test
     */
    public function deleteCategoryTest()
    {
        $complaintOld = Complaint::factory()->create();
        $response = $this->delete(route('complaints.destroy', [$complaintOld->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(Complaint::class, [
            'id' => $complaintOld->id
        ]);
    }
}
