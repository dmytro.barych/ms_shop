<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Voucher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class VoucherControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeVoucherTest()
    {
        $voucherArray = Voucher::factory()->make()->toArray();
        $response = $this->post(route('vouchers.store', $voucherArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $voucher = $response->getOriginalContent();
        $this->assertDatabaseHas(Voucher::class, [
            'id' => $voucher->id,
        ]);
    }

    /**
     * @test
     */
    public function updateVoucherTest()
    {
        $voucherOld = Voucher::factory()->create();
        $voucherArray = [
            'discount' => $this->faker->numberBetween(5,20),
            'sign' => '%',
            'product' => $voucherOld->product
        ];;
        $response = $this->put(route('vouchers.update', [$voucherOld->id] + $voucherArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Voucher::class, [
            'id' => $voucherOld->id,
            'discount' => Str::slug($voucherArray['discount']),
        ]);
    }

    /**
     * @test
     */
    public function indexVoucherTest()
    {
        Voucher::factory()->count(5)->create();
        $response = $this->get(route('vouchers.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'code',
                    'type',
                    'discount',
                    'product'
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function showVoucherTest()
    {
        $voucher = Voucher::factory()->create();
        $response = $this->get(route('vouchers.show', [$voucher->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'code',
                'type',
                'discount',
                'product'
            ]
        ]);
    }

    /**
     * @test
     */
    public function deleteVoucherTest()
    {
        $voucher = Voucher::factory()->create();
        $response = $this->delete(route('vouchers.destroy', [$voucher->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(Voucher::class, [
            'id' => $voucher->id
        ]);
    }
}
