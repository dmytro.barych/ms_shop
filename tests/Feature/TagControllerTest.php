<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class TagControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function indexTagTest()
    {
        $productArray = [
            'status' => Product::AVAILABLE_STATUS,
            'title' => $this->faker->jobTitle,
            'description' => $this->faker->text,
            'category_ids' => [Category::factory()->create()->id],
            'tags' => ['test', 'test2'],
            'price' => $this->faker->randomDigitNotZero(),
            'currency' => 'USD'
        ];
        $response = $this->post(route('products.store', $productArray));
        $response->assertStatus(Response::HTTP_CREATED);

        $response = $this->get(route('tags.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }
}
