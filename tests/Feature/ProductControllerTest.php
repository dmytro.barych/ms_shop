<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeProductTest()
    {
        $productArray = [
            'status' => Product::AVAILABLE_STATUS,
            'title' => $this->faker->jobTitle,
            'description' => $this->faker->text,
            'category_ids' => [Category::factory()->create()->id],
            'tags' => ['test'],
            'price' => $this->faker->randomDigitNotZero(),
            'currency' => 'USD'
        ];
        $response = $this->post(route('products.store', $productArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $product = $response->getOriginalContent();
        $this->assertDatabaseHas(Product::class, [
            'id' => $product->id,
        ]);
    }

    /**
     * @test
     */
    public function updateProductTest()
    {
        $productOld = Product::factory()->create(['status' => Product::AVAILABLE_STATUS]);
        $productArray = [$productOld->id, 'status' => Product::END_SOON_STATUS];
        $response = $this->put(route('products.update', $productArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Product::class, [
            'id' => $productOld->id,
            'status' => Product::END_SOON_STATUS,
        ]);
    }

    /**
     * @test
     */
    public function indexProductTest()
    {
        Product::factory()->count(3)->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'description',
                    'price',
                    'currency',
                    'rate',
                    'created_at'
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function showProductTest()
    {
        $product = Product::factory()->create();
        $response = $this->get(route('products.show', [$product->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'description',
                'price',
                'currency',
                'rate',
                'created_at'
            ]
        ]);
    }

    /**
     * @test
     */
    public function deleteProductTest()
    {
        $productOld = Product::factory()->create();
        $response = $this->delete(route('products.destroy', [$productOld->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(Product::class, [
            'id' => $productOld->id
        ]);
    }
}
