<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Voucher;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storeOrderTest()
    {
        $product = Product::factory()->create(['price' => 100, 'currency' => 'USD', 'type' => Product::TYPE_A]);
        $voucher = Voucher::factory()->create(
            [
                'product' => Product::TYPE_A,
                'type' => Voucher::TYPE_V,
                'discount' => 5,
                'sign' => '%',
            ]
        );
        $orderArray = [
            'info' => $this->faker->text,
            'user_id' => 2,
            'voucher_codes' => [
                $voucher->code
            ],
            'product_ids' => [
                $product->id,
                $product->id
            ],
        ];
        $response = $this->post(route('orders.store', $orderArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $order = $response->getOriginalContent();
        // 100+ 100 - (5% on each second product of type a) = 195
        $this->assertDatabaseHas(Order::class, [
            'id' => $order->id,
            'sum' => 195.0
        ]);
        $this->assertDatabaseHas(OrderItem::class, [
            'order_id' => $order->id,
        ]);
    }

    /**
     * @test
     * @depends storeOrderTest
     */
    public function indexOrderTest()
    {
        $response = $this->get(route('orders.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'user_id',
                    'info',
                    'items'
                ]
            ]
        ]);
    }

    /**
     * @test
     * @depends storeOrderTest
     */
    public function showCategoryTest()
    {
        $order = Order::first();
        $response = $this->get(route('orders.show', [$order->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'user_id',
                'info',
                'items'
            ]
        ]);
    }

    /**
     * @test
     */
    public function deleteCategoryTest()
    {
        $order = Order::first();
        $response = $this->delete(route('orders.destroy', [$order->id]));
        $response->assertStatus(Response::HTTP_OK);
        $order->refresh();
        $this->assertNotNull($order->deleted_at);
    }
}
