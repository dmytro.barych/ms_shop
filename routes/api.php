<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductLikeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\VoucherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('ms-gate')->group(function () {
    Route::apiResource('products', ProductController::class);
    Route::post('products/{product}/toggle-like/{userId}', [ProductLikeController::class, 'toggleLike'])
        ->name('products.toggle-like');
    Route::post('wish-list/{userId}', [ProductLikeController::class, 'wishList'])->name('products.wish-list');

    Route::apiResource('categories', CategoryController::class);
    Route::get('categories/{category}/products', [ProductController::class, 'getProductsByCategory'])
        ->name('categories.products');
    Route::apiResource('tags', TagController::class)->only(['index']);
    Route::get('tags/{tag}/products', [ProductController::class, 'getProductsByTag'])->name('tags.products');

    Route::get('comments/{comment}/childes', [CommentController::class, 'showChildes'])
        ->name('comments.childes');
    Route::apiResource('comments', CommentController::class);

    Route::post('comments/{comment}/toggle-like/{userId}', [CommentController::class, 'toggleLike'])
        ->name('comments.toggle-like');

    Route::apiResource('orders', OrderController::class)->except(['update']);
    Route::apiResource('vouchers', VoucherController::class);
    Route::post('complaints/{complaint}/toggle-check', [ComplaintController::class, 'toggleChecked'])
        ->name('complaints.toggle-check');
    Route::apiResource('complaints', ComplaintController::class)->except(['update']);
});
